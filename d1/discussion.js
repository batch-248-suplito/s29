// [SECTION] Comparison Query Operators
// $gt/$gte operator
/*
Allows us to find documents that have field number values greater than or
equal to a specified value.
Syntax:
db.collectionName.find({field : {$gt : value}});
*/

db.users.find({ age: { $gt: 50}});
db.users.find({ age: { $gte: 50}});

// $lt/$lte operator
/*
Allows us to find documents that have field number values less than or
equal to a specified value.
Syntax:
db.collectionName.find({field : {$lt : value}});
*/
db.users.find({ age: { $lt: 50}});
db.users.find({ age: { $lte: 50}});

// $ne operator
/*
Allows us to find documents that have field number values not
equal to a specified value.
Syntax:
db.collectionName.find({field : {$ne : value}});
*/
db.users.find({ age: { $ne: 82}});

// $in operator
/*
Allows us to find documents with specific match criteria using different specified value.
Syntax:
db.collectionName.find({field : {$in : value}});
*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});

// [SECTION] Logical Query Operators
// $or operator
/*
Allows us to find documents with specific match a single criteria from multiple provided search criteria.
Syntax:
db.collectionName.find({$or: [{exp..}, {epr..}]});
*/
db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

// $and operator
/*
Allows us to find documents with specific match a multiple criteria from multiple provided search criteria.
Syntax:
db.collectionName.find({$and: [{exp..}, {epr..}]});
*/
db.users.find({ $and: [ {age: {$ne: 82}}, {age: {$ne: 76}}] });

// [SECTION] Field Projection
// Inclusion
/*
Allows us to include specific field/s when retrieving documents.
The value provided is 1 to denote that the field is being included
Syntax:
db.collectionName.find({criteria}, {field: 1});
*/
db.users.find(
    {firstName: "Jane"}, 
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);

// Exclusion
/*
Allows us to Exclude specific field/s when retrieving documents.
The value provided is 0 to denote that the field is being Excluded
Syntax:
db.collectionName.find({criteria}, {field: 0});
*/
// Find the records in the db.users collection where the firstName is "Jane", and exclude the contact and department fields
db.users.find(
    {firstName: "Jane"}, 
    {
        contact: 0,
        department: 0
    }
);
// Suppresing the ID field
//This code finds the records in 'db.users' where the firstName field is equal to 'Jane'.
//It excludes the fields 'contact', 'department' and, optionally, '_id' and 'lastName'.
//Note that all exclusionary projections must include the '_id' field, otherwise an error will be thrown (errmsg: "Cannot do inclusion on field firstName in exclusion projection")
db.users.find(
    {firstName: "Jane"},
    {
        _id: 0,
        contact: 0,
        department: 0,
        lastName: 0,
    }
);

// Returning specific fields in Embedded documents
db.users.find(
    {firstName: "Jane"}, 
    {
        firstName: 1,
        lastName: 1,
        "contact.phone": 1
    }
);

// Mini activity: Return all fields except the phone field embedded in contact field.
db.users.find(
    {firstName: "Jane"},
    {
        "contact.phone": 0
    }
);

//Evaluation query operators
/*
 $regex operator
 Allows us to find documents where values match a specified regular expression.
Syntax:
db.users.find({ field: { $regex: 'pattern',
$options: '$optionVa1ue' } });
*/
//Case sensitive Query
db.users.find({firstName: {$regex: 'N'}});

//Case insensitive Query
db.users.find({firstName: {$regex: 'j', $options: '$i'}});